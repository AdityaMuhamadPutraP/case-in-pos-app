@extends('templates.header')

@section('content')
<div class="card">
  <div class="card-header">
    <h5>Data Pembelian</h5>
  </div>
  <div class="px-3 pt-3">
    <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#purchasesModal">
        + Tambah
    </button>
    <table class="table table-striped table-bordered table-hovered">
        <thead>
            <th>No</th>
            <th>Faktur</th>
            <th>Supplier</th>
        </thead>
    </table>
  </div>
</div>
@endsection
@include('modals.purchases')
@section('scripts')
<script>

    $(function () {
        $(document).on('change', '#product_id', function () {
            let product_id =  $(this).val()
            $.ajax({
                url: "{{ url('/') }}/purchases/findProduct/"+product_id,
                method: 'POST',
                data: {
                    product_id: product_id,
                    _token: '{{ csrf_token() }}'
                },
                dataType: 'json',
                success:function(response) {
                    console.log(response)
                    let maxStokHidden = response.data.stok 
                    $("#maxStokHidden").val(maxStokHidden)
                    $("#maxStokHidden").attr('max', maxStokHidden)
                }
            })
        })
        $(document).on('keyup', '#quantity', function () {
            let maxStokHidden = parseInt($("#maxStokHidden").val())
            let quantity = parseInt($(this).val())

            if (quantity > maxStokHidden) {
                Swal.fire({
                    icon: 'info',
                    title: 'Quantity melebihi batas stok!',
                    showConfirmButton: true,
                })
                $(this).val($("#maxStokHidden").val())
            }
        })
    })

</script>
@endsection