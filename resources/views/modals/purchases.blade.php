<div class="modal fade" id="purchasesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Pembelian</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="">
            <div class="form-group mb-3">
                <label class="mb-2" for="">Pilih Produk</label>
                <select name="product_id" id="product_id" class="form-control">
                    <option value="">-- Pilih --</option>
                    @foreach($products as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-3">
                <label class="mb-2" for="">Quantity</label>
                <input type="number" class="form-control" id="quantity">
                <input type="hidden" class="form-control" id="maxStokHidden">
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>